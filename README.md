### RE Manager

This image is used by the queueserver. It creates the environment required to run the RE manager. 

You might start a Queserver with some compose file like this. 

```
version: "3.7"
services:
  redis:
    env_file: .env
    image: redis:latest
    ports:s
      - 6379:6379
  re-manager:
    image: queueservercontainer-re-manager:latest
    ports:
      - 60615:60615
      - 60625:60625
    depends_on:
      - "redis"
      - "zmq-proxy"
    volumes:  
      - ./start_re.sh:/opt/start_re.sh
      - ./startup.py:/opt/startup.py
      - ./user_group_permissions.yaml:/opt/user_group_permissions.yaml
    working_dir: /opt
    command: /opt/start_re.sh
  zmq-proxy:
    env_file: .env
    build: ./zmq-proxy
    ports:
      - 5577:5577
      - 5578:5578
    command: bluesky-0MQ-proxy 5577 5578
```