FROM python:3.10-slim-bullseye
ENV TZ=Europe/Berlin


# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    libgl1-mesa-dri \
    libgl1-mesa-dri \
    tzdata \
    python3 \
    python3-pip \
    re2c \
    rsync \
    ssh-client \
    nano \
    build-essential
    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev 

RUN pip install --upgrade pip


# Install Python packages required for Bluesky and scientific computing.
RUN python3 -m pip install --upgrade bluesky ophyd matplotlib ipython pyepics bluesky-queueserver wheel


# Install tiled 
RUN python3 -m pip install --upgrade databroker[all]==2.0.0b30


#COPY ./startup.py ./startup.py

#COPY ./user_group_permissions.yaml ./user_group_permissions.yaml
#COPY ./start_re.sh ./start_re.sh


